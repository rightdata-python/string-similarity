from fuzzy_match import algorithims
from fuzzy_match import match
import Levenshtein as lev
import pandas as pd
import databricks.koalas as ks
from pyspark.sql import SparkSession

spark = SparkSession.builder.appName("Spark Linear Regression Test").getOrCreate()
print("SparkSession Starter")


# function that calculates string similarity Distance and processes other inputs when the API runs
def str_similaritiesDef(inputFile, columnName1, columnName2, outputFile, threshold, bToLower, bToUpper, sort, sToInclude,
                        algorithms):
    df = spark.read.option("header", True).csv(inputFile)  # read input data as spark tables
    print(df.count())  # prints no. of rows in the DataFrame
    dataset = df.toPandas()  # converting to pandas dataframe
    dataset = dataset.astype(str)  # making the whole dataset as string

    columnsToInclude = [columnName1, columnName2]

    # using the apply and lambda function to calculate the Levenshtein distance by calling calculateLevenshtein
    # function between columnName1 and columnName2
    if 'lev' in algorithms:
        dataset["Levenshtein"] = dataset[[columnName1, columnName2]].apply(lambda row: calculateLevenshtein(*row),
                                                                           axis=1)
        columnsToInclude.append('Levenshtein')
        if sort:
            dataset = dataset.sort_values(by=['Levenshtein'], ascending=False)
        dataset["Levenshtein"] = dataset["Levenshtein"].astype(float)

    # using the apply and lambda function to calculate the Jaro-Winkler distance by calling calculateJaro-Winkler
    # function between columnName1 and columnName2
    if 'jar' in algorithms:
        dataset["Jaro_Winkler"] = dataset[[columnName1, columnName2]].apply(lambda row: calculateJaroWinkler(*row),
                                                                            axis=1)
        columnsToInclude.append('Jaro_Winkler')
        if sort:
            dataset = dataset.sort_values(by=['Jaro_Winkler'], ascending=False)
        dataset["Jaro_Winkler"] = dataset["Jaro_Winkler"].astype(float)

    # using the apply and lambda function to calculate the Cosine distance by calling calculateCosine
    # function between columnName1 and columnName2
    if 'cos' in algorithms:
        dataset["Cosine"] = dataset[[columnName1, columnName2]].apply(lambda row: calculateCosine(*row),
                                                                       axis=1)
        columnsToInclude.append('Cosine')
        if sort:
            dataset = dataset.sort_values(by=['Cosine'], ascending=False)
        dataset["Cosine"] = dataset["Cosine"].astype(float)

    # using the apply and lambda function to calculate the Trigram distance by calling calculateTrigram
    # function between columnName1 and columnName2
    if 'tri' in algorithms:
        dataset["Trigram"] = dataset[[columnName1, columnName2]].apply(lambda row: calculateTrigram(*row),
                                                                       axis=1)
        columnsToInclude.append('Trigram')
        if sort:
            dataset = dataset.sort_values(by=['Trigram'], ascending=False)
        dataset["Trigram"] = dataset["Trigram"].astype(float)

    # if statement to verify user API inputs to convert the final data into lowerCase or upperCase
    if bToLower:
        dataset[columnName1] = dataset[columnName1].str.lower()
        dataset[columnName2] = dataset[columnName2].str.lower()
    if bToUpper:
        dataset[columnName1] = dataset[columnName1].str.upper()
        dataset[columnName2] = dataset[columnName2].str.upper()

    # if statement to print additional columns to the outputFile specified by the user API inputs
    for y in sToInclude:
        columnsToInclude.append(y)
    df = dataset[columnsToInclude]
    # print(df.dtypes)
    # query on the dataFrame to print values >= threshold value in outputFile
    if 'lev' in algorithms:
        df = df.query(f'Levenshtein >= {threshold}')
    if 'jar' in algorithms:
        df = df.query(f'Jaro_Winkler >= {threshold}')
    if 'tri' in algorithms:
        df = df.query(f'Trigram >= {threshold}')
    if 'cos' in algorithms:
        df = df.query(f'Cosine >= {threshold}')
    # print(df)
    # print DataFrame to output csv file
    df.to_csv(outputFile)


# calculates the Trigram algorithm given two strings
def calculateTrigram(s1, s2):
    score = algorithims.trigram(s1, s2)
    return score


# calculates the Levenshtein algorithm given two strings
def calculateLevenshtein(s1, s2):
    score = lev.ratio(s1, s2)
    return score


# calculates the Jaro-Winkler algorithm given two strings
def calculateJaroWinkler(s1, s2):
    score = algorithims.jaro_winkler(s1, s2)
    return score


# calculates the Cosine algorithm given two strings
def calculateCosine(s1, s2):
    score = algorithims.cosine(s1, s2)
    return score

# test case for Jaro-Winkler algorithm
# str_similaritiesDef('buyerNames.csv', 'buyer', 'buyer2', 'outputJaroWinkler.csv', .5, False, False, ['count'],
#                     ['lev', 'trigram'])
