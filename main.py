# importing required libraries and packages
import uvicorn
import fastapi
from typing import List, Optional
from fastapi import FastAPI, Query
from pydantic import BaseModel
# from str_similarities import str_similaritiesDef
from strTest import str_similaritiesDef

# assigning FastAPI as app
app = FastAPI()


# Defining Base Model
class StringSimilarityRequest(BaseModel):
    # Input file
    inputFile: "str"
    # Column Name 1
    columnName1: "str"
    # Column Name 2
    columnName2: "str"
    # Output file storage location
    outputFile: "str"
    bToUpper: Optional[bool]
    bToLower: Optional[bool]
    sort: Optional[bool]
    threshold: Optional[int]
    # 'col1,col2,col3'
    sIncludeCol: Optional[list]
    algorithms: Optional[list]


@app.post("/StringSimilarityAPI")
# calling the functions with given inputs
async def StringSimilarityAPI(LR1: StringSimilarityRequest):
    print(LR1.threshold)
    Result = str_similaritiesDef(LR1.inputFile, LR1.columnName1, LR1.columnName2, LR1.outputFile, LR1.threshold or 0,
                                 LR1.bToLower, LR1.bToUpper, LR1.sort, LR1.sIncludeCol, LR1.algorithms)
    # ...
    # Runs the DynamicLevenshtein
    # ...
    return {f"{Result}"}
